import React, { useState } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import './App.scss';
import Login from './pages/login';
import Home from './pages/home';
import { authenticate } from './services/auth';


function App() {
  const [loggedIn, setLoggedIn] = useState(authenticate());

  return (
    <Router>
      <Switch>
        <Route path="/auth/login">
          <Login onLogin={setLoggedIn} />
        </Route>
        <Route path="/dashboard/home">
          {loggedIn ? <Home /> : <Redirect push to="/auth/login" />}
        </Route>
        <Route path="/" exact>
          {loggedIn ? <Redirect to="/dashboard/home" /> : <Redirect to="/auth/login" />}
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
