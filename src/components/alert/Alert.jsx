import React from 'react';
import PropTypes from 'prop-types';
import { Severity } from '../../consts';


const Alert = ({ severity, message }) => (
  <div className={`alert alert-${severity}`} role="alert">
    {message}
  </div>
);

Alert.propTypes = {
  severity: PropTypes.string,
  message: PropTypes.string.isRequired,
};

Alert.defaultProps = {
  severity: Severity.info,
};

export default Alert;
