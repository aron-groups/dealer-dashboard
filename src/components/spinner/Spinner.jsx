import React from 'react';
import PropTypes from 'prop-types';


const Spinner = ({ size }) => (
  <span
    className={`spinner-border spinner-border-${size}`}
    role="status"
    aria-hidden="true"
  ></span>
);

Spinner.propTypes = {
  size: PropTypes.string,
};

Spinner.defaultProps = {
  size: '',
};

export default Spinner;
