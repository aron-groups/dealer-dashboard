import React, { useState } from "react";
import { useHistory } from 'react-router-dom';
import { Formik } from 'formik';
import { Alert } from '../../components/alert';
import { Spinner } from '../../components/spinner';
import { Severity } from '../../consts';
import { login } from '../../services/api';
import { login as storeLogin } from '../../services/auth';


const States = {
  Initial: 0,
  Invalid: 0,
  Valid: 1,
  Submitting: 2,
  Succeeded: 3,
  WrongPassword: 4,
  InternalError: 5,
};


export const Login = ({ onLogin }) => {
  const [formState, setFormState] = useState(States.Initial);
  const history = useHistory();

  const handleSubmit = (e) => {
    setFormState(States.Submitting);

    login({ 'username': 'sdfsdf', 'password': 'sdfsdf' })
      .then((response) => response.data)
      .then((data) => {
        storeLogin(onLogin)(data.token);
        setFormState(States.Succeeded);
        setTimeout(() => {
          history.push('/dashboard/home');
        }, 2000);
      })
      .catch((err) => {
        console.error(err);
        if (err.data) {
          setFormState(States.WrongPassword);
          return;
        }
        setFormState(States.InternalError);
      });
  };

  return (
    <div className="row bg justify-content-center align-items-center">
      <div className="col-11 col-md-10 content-wrapper">
        <div className="row align-items-center justify-content-center wrapper">
          <div className="col-6 bg-image-side d-none d-md-block"></div>
          <div className="col-12 d-sm-none">
            <img
              src="https://via.placeholder.com/85?text=ARON"
              alt="logo for mobile size screen"
              className="logo-mobile"
            />
          </div>
          <div className="col-12 d-none d-sm-block d-md-none">
            <img
              src="https://via.placeholder.com/100?text=ARON"
              alt="logo for tablet size screen"
              className="logo-mobile"
            />
          </div>
          <div className="col-12 col-md-6">
            <div className="row form-wrapper">
              <Formik
                initialValues={{ username: '', password: '' }}
                validate={(values) => {
                  const errors = {};
                  if (!values.username) {
                    errors.username = 'نام کاربری نمی‌تواند خالی باشد.';
                  }

                  if (!values.password) {
                    errors.password = 'رمز عبور نمی‌تواند خالی باشد.';
                  }
                  return errors;
                }}
                onSubmit={handleSubmit}
              >
                {({
                  values,
                  errors,
                  touched,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isSubmitting,
                  dirty,
                  isValid,
                }) => (
                    <form className="col-12 col-sm-10 col-md-10 form" onSubmit={handleSubmit}>
                      <div className="form-group">
                        <label>نام کاربری</label>
                        <input
                          disabled={[2, 3, 5].includes(formState)}
                          dir="ltr"
                          name="username"
                          type="text"
                          className="form-control"
                          onBlur={handleBlur}
                          value={values.username}
                          onChange={handleChange}
                        />
                      </div>
                      <div className="form-group">
                        {
                          (errors.username && touched.username) ?
                            (
                              <Alert
                                severity={Severity.danger}
                                message={errors.username}
                              />
                            ) :
                            null
                        }
                      </div>
                      <div className="form-group">
                        <label>رمز عبور</label>
                        <input
                          disabled={[2, 3, 5].includes(formState)}
                          dir="ltr"
                          name="password"
                          type="password"
                          className="form-control"
                          onBlur={handleBlur}
                          value={values.password}
                          onChange={handleChange}
                        />
                      </div>
                      <div className="form-group">
                        {
                          (errors.password && touched.password) ?
                            (
                              <Alert
                                severity={Severity.danger}
                                message={errors.password}
                              />
                            ) :
                            null
                        }
                      </div>
                      <div className="form-group">
                        {
                          formState > States.Submitting ?
                            (
                              <Alert
                                severity={
                                  formState === States.Succeeded ? Severity.success : Severity.danger
                                }
                                message={
                                  [
                                    'خوش آمدید. به پنل منتقل می‌شوید...',
                                    'نام کاربری یا رمز عبور اشتباه است.',
                                    'متاسفانه خطایی رخ داده است. مجدد امتحان کنید.'
                                  ][formState - States.Succeeded]
                                }
                              />
                            ) :
                            (null)
                        }
                      </div>
                      <div className="form-row justify-content-center">
                        <div className="col-8">
                          <button
                            disabled={!(dirty && isValid) || isSubmitting}
                            className="btn btn-primary btn-block"
                            type="submit"
                            onClick={handleSubmit}
                          >
                            {
                              formState === States.Submitting ? <Spinner size="sm" /> : (null)
                            }
                            {
                              formState === States.Submitting ?
                                (<span>در حال بررسی...</span>) :
                                (<span>ورود</span>)
                            }
                          </button>
                          <button
                            disabled={isSubmitting}
                            className="btn btn-link btn-block"
                            type="button"
                          >
                            <span>کاربر جدید هستید؟</span>
                          </button>
                        </div>
                      </div>
                    </form>
                  )}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    </div >
  );
};
