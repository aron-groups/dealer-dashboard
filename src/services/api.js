import axios from 'axios';
import config from '../config';


const api = axios.create({
  baseURL: config.api.baseURL,
  timeout: 3000,
});


export const login = ({ username, password }) => api.post(config.api.login, { mobile: username, password });
