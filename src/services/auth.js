import { storeToken, retrieveToken } from './storage';

export const login = (dispatch) => (token) => {
  storeToken(token);
  dispatch(true);
};

export const authenticate = () => {
  return !!retrieveToken();
};
