export const storeToken = (token) => localStorage.setItem('aron_dealing_auth_token', token);
export const retrieveToken = () => localStorage.getItem('aron_dealing_auth_token');
